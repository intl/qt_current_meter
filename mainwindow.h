#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QtCharts>
#include <QtSerialPort/QSerialPort>
#include <QtSerialPort/QSerialPortInfo>
#include "jsmn/jsmn.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT
public slots:
    /**
     * @brief connect_btn_click Обработчик нажатия кнопки "Подключить"
     */
    void connect_btn_click();
    /**
     * @brief serial_rx_data Обработчик получения данных с порта
     */
    void serial_rx_data();
    /**
     * @brief zoom_in_btn_click Событие на кнопку увелечение размера
     */
    void zoom_reset_btn_click();

    /**
     * @brief i_axis_change_range Событие на изменение маштаба для тока
     */
    void i_axis_change_range(int index);

    /**
     * @brief u_axis_change_range Событие на изменение маштаба для напряжения
     */
    void u_axis_change_range(int index);
public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

    QChartView *chart_view;
    QValueAxis* i_axis_y;
    QValueAxis* u_axis_y;
    QValueAxis* axis_x;

    QChart *chart; /**< График */
    QLineSeries *u_bus_data; /**< Данные для графика по Ubus (напряжение шины) */
    QLineSeries *u_sh_data; /**< Данные для графика по Ush (Напряжение на шунте) */
    QLineSeries *u_load_data; /**< Данные для графика по Uload (Напряжение нагрузки) */
    QLineSeries *i_data; /**< Данные для графика по I (току) */

    double time_counter; /**< Счетчик времени, в секундах */
    double u_max_range; /**< Максимальное значение данных для напряжения */
    double u_min_range; /**< Минимальное значение данных для напряжения */
    double i_min_range; /**< Минимальное значение данных для тока */
    double i_max_range; /**< Максимальное значение данных для тока */

private:
    Ui::MainWindow *ui;
    QSerialPort* serial; /**< Класс последовательного порта */

    /**
     * @brief The range_t struct
     * Структура описывающая маштабы
     * min - Минимальное значение
     * max - Максимальное значение
     */
    struct range_t
        {
        double max;
        double min;
        QString name;
        };

    static const uint32_t status_bar_timeout = 10000; /**< Таймаут для сообщений в статус-баре */
    static const range_t i_axis_ranges[4]; /**< Возможные маштабы для тока {1ма,-1ма}, {10ма,-10} {100ма,-100} {1000ма,-1000} */
    static const range_t u_axis_ranges[3]; /**< Возможные маштабы для тока {1000мв,-1000мв}, {5000мв,-5000м} {10000мв,-10000м} */

    double time_counter_range;
    /**
     * @brief MainWindow::json_eq Парсилка, сравнивает текст полученный в JSON
     * @param json Текст самого json-а
     * @param tok Токен JSMN
     * @param s Текст с которым сранивается
     * @return 0 - если совпало
     */
    int json_eq(const char *json, jsmntok_t *tok, const char *s);

};

#endif // MAINWINDOW_H
