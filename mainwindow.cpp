#include "mainwindow.h"
#include "ui_mainwindow.h"

const MainWindow::range_t MainWindow::i_axis_ranges[4] = {{1,-1,"1ма"},{10,-10,"10ма"},{100,-100,"100ма"},{1000,-1000,"1000ма"}};
const MainWindow::range_t MainWindow::u_axis_ranges[3] = {{1000,-1000,"1 В"},{5000,-5000,"5 В"},{10000,-10000,"10 В"}};

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    ui->zoom_reset_btn->setIcon(QIcon(":/zoom_best_fit.ico"));
    setWindowFlags(Qt::Window | Qt::WindowCloseButtonHint | Qt::MSWindowsFixedSizeDialogHint);
    this->setFixedSize(500,300);
    serial = new QSerialPort(this);
    connect(serial,&QSerialPort::readyRead,this,&MainWindow::serial_rx_data);
    foreach (const QSerialPortInfo &info, QSerialPortInfo::availablePorts())
       {
       ui->port_name_box->addItem(info.portName());
       }

       for(uint8_t i = 0; i<(sizeof(i_axis_ranges)/sizeof(range_t)); i++)
            {
            ui->i_axis_range_box->addItem(i_axis_ranges[i].name);
            }

       for(uint8_t i = 0; i<(sizeof(u_axis_ranges)/sizeof(range_t)); i++)
            {
            ui->u_axis_range_box->addItem(u_axis_ranges[i].name);
            }

       i_max_range = i_axis_ranges[0].max;
       i_min_range = i_axis_ranges[0].min;
       u_max_range = u_axis_ranges[0].max;
       u_min_range = u_axis_ranges[0].min;

       connect(ui->i_axis_range_box,SIGNAL(currentIndexChanged(int)),this,SLOT(i_axis_change_range(int)));
       connect(ui->u_axis_range_box,SIGNAL(currentIndexChanged(int)),this,SLOT(u_axis_change_range(int)));

       time_counter = 0;
       chart = new QChart();

       u_bus_data = new QLineSeries(); /**< Напряжение на шине будет красным цветом */
       u_bus_data->setName("Ubus");
       u_bus_data->setUseOpenGL();
       u_bus_data->setColor(Qt::red);

       u_sh_data = new QLineSeries(); /**< Напряжение на шунте будет желтым */
       u_sh_data->setName("Ush");
       u_sh_data->setUseOpenGL();
       u_sh_data->setColor(Qt::darkYellow);

       u_load_data = new QLineSeries(); /**< Напряжение нагрузки зеленым */
       u_load_data->setName("Uload");
       u_load_data->setUseOpenGL();
       u_load_data->setColor(Qt::green);

       i_data = new QLineSeries(); /**< Ток - синим */
       i_data->setName("I");
       i_data->setUseOpenGL();
       i_data->setColor(Qt::blue);
       chart->setMargins(QMargins(0,0,0,0));
       chart->layout()->setContentsMargins(0,0,0,0);

       i_axis_y = new QValueAxis();
       i_axis_y->setTitleText("Ток (мА)");
       i_axis_y->setRange(i_axis_ranges[ui->i_axis_range_box->currentIndex()].min,i_axis_ranges[ui->i_axis_range_box->currentIndex()].max);
       time_counter_range = 10;
       axis_x = new QValueAxis();
       axis_x->setRange(0,time_counter_range);
       axis_x->setTitleText("Время (с)");
       u_axis_y = new QValueAxis();
       u_axis_y->setRange(0,u_max_range);
       u_axis_y->setTitleText("Напряжение (мВ)");

       chart->addAxis(i_axis_y,Qt::AlignRight);
       chart->addAxis(u_axis_y,Qt::AlignLeft);
       chart->addAxis(axis_x,Qt::AlignBottom);

       chart->addSeries(u_bus_data);
       chart->addSeries(u_sh_data);
       chart->addSeries(u_load_data);
       chart->addSeries(i_data);

       i_data->attachAxis(i_axis_y);
       i_data->attachAxis(axis_x);
       u_bus_data->attachAxis(u_axis_y);
       u_bus_data->attachAxis(axis_x);
       u_sh_data->attachAxis(u_axis_y);
       u_sh_data->attachAxis(axis_x);
       u_load_data->attachAxis(u_axis_y);
       u_load_data->attachAxis(axis_x);

       chart->legend()->setVisible(true);
       chart->legend()->setAlignment(Qt::AlignBottom);
       chart->legend()->detachFromChart();
       chart->legend()->setGeometry(60,0,200,25);

       chart_view = new QChartView(chart);
       chart_view->setRenderHint(QPainter::Antialiasing);
       chart_view->setRubberBand(QChartView::HorizontalRubberBand);
       ui->chart_l->addWidget(chart_view);
}

MainWindow::~MainWindow()
    {
    serial->close();
    delete ui;
    }


int MainWindow::json_eq(const char *json, jsmntok_t *tok, const char *s)
    {
    if (tok->type == JSMN_STRING && (int) strlen(s) == tok->end - tok->start &&
            strncmp(json + tok->start, s, tok->end - tok->start) == 0)
        {
        return 0;
        }
    return -1;
    }

void MainWindow::zoom_reset_btn_click()
   {
   chart->zoomReset();
   }

void MainWindow::i_axis_change_range(int index)
    {
    i_min_range = i_axis_ranges[index].min;
    i_max_range = i_axis_ranges[index].max;
    i_axis_y->setRange(i_min_range,i_max_range);
    }

void MainWindow::u_axis_change_range(int index)
    {
    u_min_range = u_axis_ranges[index].min;
    u_max_range = u_axis_ranges[index].max;
    u_axis_y->setRange(u_min_range,u_max_range);

    }

void MainWindow::serial_rx_data()
   {
   if(serial->bytesAvailable() >= sizeof("{\"Ubus\":\"01036.00\",\"Ush\":\"00006.13\",\"Uload\":\"01042.13\",\"I\":\"00061.60\"}"))
          {
           QByteArray data = serial->readAll();
           char* serial_data =  data.data();
           ui->event_box->showMessage(QString::fromLatin1(data.data()));
           jsmn_parser p;
           jsmntok_t t[128];
           jsmn_init(&p);

           int r = jsmn_parse(&p,serial_data, data.length(), t, sizeof(t)/sizeof(t[0]));
           if (r < 0)
                {
                return;
                }

           if (r < 1 || t[0].type != JSMN_OBJECT)
                {
                return;
                }


           for (uint8_t i = 1; i < r; i++)
                {
                if (json_eq(serial_data, &t[i], "Ubus") == 0)
                   {
                   QString d = QString::fromLocal8Bit(serial_data + t[i+1].start,t[i+1].end-t[i+1].start);
                   bool err = true;
                   float df = d.toFloat(&err);
                   if(err)
                       {
                       u_bus_data->append(time_counter,df);

                       if(df>u_max_range)
                          {
                          u_max_range = df * 1.1;
                          }

                       if(df < u_min_range)
                        {
                        if(df>0)
                            u_min_range = df/1.1;
                        if(df<0)
                            u_min_range = df*1.1;
                        }

                       }
                   continue;
                   }
                if (json_eq(serial_data, &t[i], "Ush") == 0)
                   {
                   QString d = QString::fromLocal8Bit(serial_data + t[i+1].start,t[i+1].end-t[i+1].start);
                   bool err = true;
                   float df = d.toFloat(&err);
                   if(err)
                        {
                        u_sh_data->append(time_counter,df);    
                        if(df>u_max_range)
                           {
                           u_max_range = df * 1.1;
                           }

                        if(df < u_min_range)
                         {
                         if(df>0)
                             u_min_range = df/1.1;
                         if(df<0)
                             u_min_range = df*1.1;
                         }
                        }
                    i++;
                   continue;
                   }
                if (json_eq(serial_data, &t[i], "Uload") == 0)
                   {
                    QString d = QString::fromLocal8Bit(serial_data + t[i+1].start,t[i+1].end-t[i+1].start);
                    bool err = true;
                    float df = d.toFloat(&err);
                    if(err)
                         {
                         u_load_data->append(time_counter,df);

                         if(df>u_max_range)
                            {
                            u_max_range = df * 1.1;
                            }

                         if(df < u_min_range)
                          {
                          if(df>0)
                              u_min_range = df/1.1;
                          if(df<0)
                              u_min_range = df*1.1;
                          }

                         }
                    i++;
                    continue;
                   }
                if (json_eq(serial_data, &t[i], "I") == 0)
                   {
                    QString d = QString::fromLocal8Bit(serial_data + t[i+1].start,t[i+1].end-t[i+1].start);
                    bool err = true;
                    float df = d.toFloat(&err);
                    if(err)
                         {
                         bool range_change = false;
                         if(df > i_max_range)
                            {
                            i_max_range = df * 1.1;
                            range_change = true;
                            }
                         if(df < i_min_range)
                            {
                            if(i_min_range > 0)
                                i_min_range = 0;
                            else
                                i_min_range = df*1.1;
                            range_change = true;
                            }

                         if(range_change)
                            {
                            i_axis_y->setRange(i_min_range,i_max_range);
                            }

                         i_data->append(time_counter,df);

                         }
                     i++;
                    continue;
                   }
                }

           u_axis_y->setRange(u_min_range,u_max_range);
           i_axis_y->setRange(i_min_range,i_max_range);
           if(time_counter>time_counter_range)
                {
                chart->zoomReset();
                time_counter_range *= 2;
                axis_x->setRange(0,time_counter_range);
                }
           time_counter+=0.1;
           }
   }

void MainWindow::connect_btn_click()
    {
     if(serial->isOpen())
          {
          serial->clear();
          serial->close();
          ui->event_box->showMessage(("Соединение закрыто"),status_bar_timeout);
          ui->connect_btn->setText("Подключить");
          return;
          }

    u_bus_data->clear();
    u_sh_data->clear();
    u_load_data->clear();
    i_data->clear();
    time_counter = 0;
      /**
       * Открываем порт 9600:8n1
       */
      serial->setPortName(ui->port_name_box->currentText());
      serial->setBaudRate(QSerialPort::Baud9600);
      serial->setDataBits(QSerialPort::Data8);
      serial->setFlowControl(QSerialPort::NoFlowControl);
      serial->setParity(QSerialPort::NoParity);
      if(serial->open(QIODevice::ReadWrite))
          {
          ui->event_box->showMessage(("Порт "+ui->port_name_box->currentText()+" открыт"),status_bar_timeout);
          ui->connect_btn->setText("Отключить");
          }
      else
          {
          ui->event_box->showMessage("Ошибка: Невозможно открыть порт "+ui->port_name_box->currentText(),status_bar_timeout);
          }
    }
